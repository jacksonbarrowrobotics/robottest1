/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.wpi.first.wpilibj.templates.subsystems;

import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.RobotDrive;

/**
 *
 * @author hal
 */
public class DriveTrain extends Subsystem {
    RobotDrive myChassis;
    // Put methods for controlling this subsystem
    // here. Call these from Commands.

    public void initDefaultCommand() {
        // Set the default command for a subsystem here.
        //setDefaultCommand(new MySpecialCommand());
    }
    public DriveTrain(){
        myChassis = new RobotDrive(1,2);
    }
    public void tankDrive(double left, double right){
        myChassis.tankDrive(left, right);
    
    }
}